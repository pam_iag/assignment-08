#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct students{
    char fname[20];
    char subject[20];
    int marks;
};
int main()
{
    struct students s[5];
    int i;

    for (i=0; i<5; i++)
    {
        printf("Enter the first name: ");
        scanf("%s", &s[i].fname);

        printf("Enter subject: ");
        scanf("%s", &s[i].subject);

        printf("Enter marks: ");
        scanf("%d", &s[i].marks);

        printf("-----------\n");
    }

    for (i=0; i<5; i++)
    {
        printf("\nFirst Name: %s\n", s[i].fname);
        printf("Subject: %s\n", s[i].subject);
        printf("Marks: %d", s[i].marks);
        printf("\n");
    }
    return 0;
}
